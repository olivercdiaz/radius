# RADIUS

**Objectius**

L'objectiu d'aquesta pràctica és configurar un servidor FreeRADIUS bàsic i provar l'autenticació RADIUS. També configuraqr FreeRADIUS de PfSense.

**Configuració bàsica de servidor RADIUS amb FreeRADIUS (directament)**

1. Crea un equip virtual amb Ubuntu Server i instal·la i configura el servidor RADIUS FreeRADIUS.

`Toda la instalación y configuración lo hice desde un ssh en mi máquina anfitriona conectada al ubuntu server.`

- Instalamos "FreeRADIUS"
![](imagenes/1.png)

- Abrimos puertos
![](imagenes/2.png)

2. Fes la prova que el servidor està escoltant des d’un client Ubuntu (connectat a la mateixa xarxa) amb la comanda “radtest”.
   ![](imagenes/3.png)
   ![](imagenes/4.png)

3. Connecta un client al servidor RADIUS per comprovar que valida les credencials. Recorda que aquest client és normalment un NAS o Network Access Server que podria ser un ount d'accés Wifi, un sevidor VPN, etc.

- Vamos a crear un usuario con "adduser" y dentro de /etc/FreeRADIUS/3.0/clients.conf, escribimos esto:
![](imagenes/5.png)

`systemctl restart freeradius`

![](imagenes/8.png)
![](imagenes/6.png)
![](imagenes/7.png)

**Configuració de servidor RADIUS amb PFSense**

1. Crea un equip virtual i instal·la PFSense i el mòdul FreeRADIUS. Vigileu, per que el PFSense per defecte només es pot administrar des de la interfície LAN (bloca els accessos al gestor Web i ssh que arribin per la interfície WAN).

- Instalación FreeRADIUS

![](imagenes/9.png)

- Configuración de los dos puertos

![](imagenes/10.png)

- Creamos un sistema de autenticación NAS en el PfSense

![](imagenes/12.png)

- Creamos un usuario para hacer la conexió

![](imagenes/13.png)

2. Prova des del propi Pfsense que funciona el servidor RADIUS.

![](imagenes/14.png)
